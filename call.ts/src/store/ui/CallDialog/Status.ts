import { Dispatch } from 'redux';
import { callStatusClose } from 'store/actions/CallDialog/Status';
import { AppState } from 'store/State';

interface State {
  open: boolean;
  message: string;
}

interface Actions {
  close(): void;
}

export interface Props extends State, Actions {}

export function mapStateToProps(state: AppState): State {
  return { open: state.callStatusOpen, message: state.callStatusMessage };
}

export function mapDispatchToProps(dispatch: Dispatch): Actions {
  return { close: () => dispatch(callStatusClose) };
}
