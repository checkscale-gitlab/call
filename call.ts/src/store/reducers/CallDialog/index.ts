import axios from 'axios';
import { call, put, select } from 'redux-saga/effects';
import { AppState, INITIAL_STATE } from 'store/State';
import { update } from 'store/utils';
import {
  callDialogClose,
  callDialogSendFail,
  callDialogSendSuccess,
} from 'store/actions/CallDialog';
import { Actions, CallDialogAction } from 'store/actions';

export function callDialogReducer(
  state = INITIAL_STATE,
  action: CallDialogAction
): AppState {
  switch (action.type) {
    case Actions.CALL_DIALOG_OPEN:
      return update(state, { callDialogOpen: true });
    case Actions.CALL_DIALOG_CLOSE:
      return update(state, { callDialogOpen: false });
    case Actions.CALL_DIALOG_SEND:
      return update(state, { callDialogIsSending: true });
    case Actions.CALL_DIALOG_SEND_OK:
      return update(state, { callDialogIsSending: false });
    case Actions.CALL_DIALOG_SEND_FAIL:
      return update(state, {
        callFormError: action.message,
        callDialogIsSending: false,
      });
    default:
      return state;
  }
}

function encodeForm(name: string, phone: string) {
  return `name=${encodeURIComponent(name)}&phone=${encodeURIComponent(phone)}`;
}

async function sendCallNotificationRequest(
  name: string,
  phone: string
): Promise<string> {
  const response = await axios.post('/api/notify', encodeForm(name, phone), {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    validateStatus: (status: number) => status < 500,
  });

  const responseText = response.data as string;
  if (response.status !== 200) {
    throw new Error(responseText);
  }

  return responseText;
}

export function* sendCallNotification(action: CallDialogAction) {
  const [name, phone] = yield select((state: AppState) => [
    state.callFormName,
    state.callFormPhone,
  ]);

  if (name === '' || phone === '') {
    yield put(callDialogSendFail('Fields are not optional!'));
    return;
  }

  try {
    const status = yield call(() => sendCallNotificationRequest(name, phone));
    yield put(callDialogClose);
    yield put(callDialogSendSuccess(status));
  } catch (error) {
    yield put(callDialogSendFail(error.message));
  }
}
